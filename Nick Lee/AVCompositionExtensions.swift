//
//  AVCompositionExtensions.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import AVFoundation

private let kDefaultNumberOfLoops = 2048

extension AVMutableComposition {
    
    convenience init?(asset: AVAsset, loops: Int = kDefaultNumberOfLoops) {
        
        self.init()

        let assetDuration = asset.duration
        let startTime = CMTime(value: 0, timescale: assetDuration.timescale, flags: assetDuration.flags, epoch: assetDuration.epoch)
        let endTime = assetDuration
        let range = CMTimeRange(start: startTime, duration: endTime)
        
        let results = (0..<loops).map { (_) -> Bool in self.insertTimeRange(range, ofAsset: asset, atTime: self.duration, error: nil) }
        
        let result = results.reduce(true) { $0 && $1 }
        
        if !result {
            return nil
        }
        
    }
    
}