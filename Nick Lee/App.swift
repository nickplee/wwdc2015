//
//  App.swift
//  Nick Lee
//
//  Created by Nick on 4/25/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import Foundation
import UIKit

class App: Content {
    
    // MARK: Public properties
    
    let icon: UIImage
    let name: String
    let description: String
    let appleStoreID: String
    
    init(icon: UIImage, name: String, description: String, appleStoreID: String, headerLabel: String, headerValue: String, height: CGFloat) {
        self.icon = icon
        self.name = name
        self.description = description
        self.appleStoreID = appleStoreID
        super.init(headerLabel: headerLabel, headerValue: headerValue, height: height)
    }
    
}