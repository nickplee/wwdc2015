//
//  BaseCell.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell {
    
    // MARK: Public properties
    
    @IBOutlet weak var headerView: CellHeaderView!
    
    // This fixes a mystery constraint issue :)
    override var bounds : CGRect {
        didSet {
            contentView.frame = bounds
        }
    }
    
}