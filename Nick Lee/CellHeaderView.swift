//
//  CellHeaderView.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import UIKit

class CellHeaderView: UIView {
    
    // MARK: Public Properties
    
    @IBInspectable var textColor: UIColor = UIColor.whiteColor() {
        didSet {
            updateAppearance()
        }
    }
    
    @IBInspectable var lineColor: UIColor = UIColor.whiteColor() {
        didSet {
            updateAppearance()
        }
    }
    
    @IBInspectable var font: UIFont = UIFont(name: "Menlo", size: 17.0)! {
        didSet {
            updateAppearance()
        }
    }
    
    @IBInspectable var label: String = "No Label" {
        didSet {
            updateAppearance()
        }
    }
    
    @IBInspectable var value: String = "No Value" {
        didSet {
            updateAppearance()
        }
    }
    
    // MARK: Private Properties
    
    private let labelLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .Left
        label.backgroundColor = UIColor.clearColor()
        label.setTranslatesAutoresizingMaskIntoConstraints(false)
        return label
    }()
    
    private let valueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .Right
        label.setTranslatesAutoresizingMaskIntoConstraints(false)
        label.backgroundColor = UIColor.clearColor()
        return label
        }()
    
    // MARK: Initialization
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        
        addSubview(labelLabel)
        addSubview(valueLabel)

        let bindings = [
            "label" : labelLabel,
            "value" : valueLabel
        ]
        
        var constraints = NSLayoutConstraint.constraintsWithVisualFormat("|[label]-[value]|", options: nil, metrics: nil, views: bindings)
        constraints += NSLayoutConstraint.constraintsWithVisualFormat("V:|[label]|", options: nil, metrics: nil, views: bindings)
        constraints += NSLayoutConstraint.constraintsWithVisualFormat("V:|[value]|", options: nil, metrics: nil, views: bindings)
        
        addConstraints(constraints)
        
        updateAppearance()
    }
    
    // MARK: Appearance
    
    private func updateAppearance() {
        
        labelLabel.text = label
        valueLabel.text = value
        
        let labels = [labelLabel, valueLabel]
        
        for l in labels {
            l.font = font
            l.textColor = textColor
        }
        
        invalidateIntrinsicContentSize()
        setNeedsDisplay()
    }
    
    // MARK: Drawing
    
    override func drawRect(rect: CGRect) {
        lineColor.setStroke()
        let context = UIGraphicsGetCurrentContext()
        let lineRect = CGRect(
            x: rect.origin.x,
            y: rect.size.height - 1.0,
            width: rect.size.width,
            height: 1.0
        )
        UIBezierPath(rect: lineRect).stroke()
    }
}
