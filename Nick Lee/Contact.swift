//
//  Contact.swift
//  Nick Lee
//
//  Created by Nick on 4/25/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import Foundation
import UIKit

class Contact: Content {
    
    // MARK: Public properties
    
    let email: String
    
    // MARK: Initialization
    
    init(email: String, headerLabel: String, headerValue: String, height: CGFloat) {
        self.email = email
        super.init(headerLabel: headerLabel, headerValue: headerValue, height: height)
    }
    
    
}