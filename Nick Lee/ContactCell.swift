//
//  ContactCell.swift
//  Nick Lee
//
//  Created by Nick on 4/25/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import UIKit

class ContactCell: BaseCell {
    
    // MARK: Public Properties
    
    @IBOutlet weak var emailLabel: UILabel!
    
}