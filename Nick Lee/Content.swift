//
//  Content.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import Foundation
import UIKit

class Content {
    
    // MARK: Public properties
    
    let headerLabel: String
    let headerValue: String
    let height: CGFloat
    
    // MARK: Initialization
    
    init(headerLabel: String, headerValue: String, height: CGFloat) {
        self.headerLabel = headerLabel
        self.headerValue = headerValue
        self.height = height
    }
    
}