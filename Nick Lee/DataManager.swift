//
//  DataManager.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import Foundation
import UIKit

private let sharedDataManager = DataManager()

class DataManager {

    
    // MARK: Public properties
    
    class var sharedInstance: DataManager {
        return sharedDataManager
    }
    
    var count: Int {
        return content.count
    }
    
    subscript(index: Int) -> Content {
        return content[index]
    }
    
    // MARK: Private properties
    
    private var content: [Content] = []
    
    // MARK: Initialization
    
    private init() {
        loadData()
    }
    
    // MARK: Loading
    
    private func loadData() {
        
        // I may want to make this dynamic someday
        // For now, it's all hardcoded
        
        content = [
            Image(image: UIImage(named: "brooklyn")!, headerLabel: "Location:", headerValue: "Brooklyn, NY", height: 350.0),
            
            Text(text: "\tI'm a 22-year-old engineering student from Brooklyn, New York, where I currently attend New York University.  My life revolves around two passions: building software and playing the piano.\n\n\tI first learned Objective-C shortly before the iOS SDK's launch in 2008, and was immediately impressed by the ease-of-use and consistency of its frameworks.  Since then, I've obtained a part-time job (while I finish school) as a software developer and have deployed a considerable number of apps, both personally and through my employer.\n\n\tMore recently, I have fallen in love with Apple's new Swift programming language.  In fact, this app was written exclusively in Swift.  Its virtues are far too numerous to name here, but, in short, its modern syntax combined with intelligent type-safety means I write safer, more reliable, and more readable code, much more of the time :)\n\n\tMost importantly, I want to thank you for considering me for this scholarship-- the knowledge and excitement put forth at WWDC are greatly inspiring to me, and I very much hope to attend this year.", headerLabel: "About Me", headerValue: "", height: 600.0),
            
            App(icon: UIImage(named: "upvote")!, name: "Upvote", description: "Upvote is a minimal, but immersive Reddit client for iOS featuring large image previews.  It was briefly the most popular free news app in the iOS app store.", appleStoreID: "626595701", headerLabel: "Featured App", headerValue: "(personal)", height: 250.0),
            
            App(icon: UIImage(named: "litely")!, name: "Litely", description: "100% Swift rebuild of this popular photography app.  Featured numerous times both on the App Store and on devices in Apple Stores.", appleStoreID: "850707754", headerLabel: "Featured App", headerValue: "(work project)", height: 250.0),
            
            App(icon: UIImage(named: "robots")!, name: "IEEE Robots", description: "Robots for iPad is the best, most complete guide to the world of robotics. This fun, highly interactive app lets you explore over 100 real-world robots, with hundreds of animations, photos, videos, and articles.", appleStoreID: "566581906", headerLabel: "Featured App", headerValue: "(work project)", height: 250.0),
            
            Text(text: "Languages: Swift, Objective-C, Javascript, Ruby, C/C++, ARM Assembly, Java, PHP\n\nWeb Development: Node.js, Rails, Sinatra, Backbone, HTML, CSS< AJAX, JQuery\n\nDatastores: MongoDB, Redis, Elasticsearch, MySQL, PostgreSQL\n\nCloud services: AWS, Heroku, DigitalOcean, Parse, iCloud/CloudKit\n\nDigital electronics: Arduino, ESP8266", headerLabel: "Skills", headerValue: "", height: 300.0),
            
            Contact(email: "nick@nickplee.com", headerLabel: "Contact", headerValue: "", height: 100.0)
        ]
        
    }
}