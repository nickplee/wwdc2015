//
//  Image.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import Foundation
import UIKit

class Image: Content {
    
    // MARK: Public properties
    
    let image: UIImage
    
    // MARK: Initialization
    
    init(image: UIImage, headerLabel: String, headerValue: String, height: CGFloat) {
        self.image = image
        super.init(headerLabel: headerLabel, headerValue: headerValue, height: height)
    }
    
}