//
//  ImageCell.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import UIKit

class ImageCell: BaseCell {
    
    // MARK: Public properties
    
    @IBOutlet weak var imageView: UIImageView!
    
}