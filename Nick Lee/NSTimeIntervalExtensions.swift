//
//  NSTimeIntervalExtensions.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import Foundation

extension NSTimeInterval {
    
    func delay(queue: dispatch_queue_t = dispatch_get_main_queue(), closure: () -> ()) {
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(Double(self) * Double(NSEC_PER_SEC)))
        dispatch_after(time, queue, closure)
    }
    
    func every(queue: dispatch_queue_t = dispatch_get_main_queue(), closure: (stop: () -> ()) -> ()) {
        if let timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue) {
            let interval = UInt64(Double(self) * Double(NSEC_PER_SEC))
            let startTime = dispatch_walltime(nil, 0)
            dispatch_source_set_timer(timer, startTime, interval, 0)
            dispatch_source_set_event_handler(timer) {
                let stop: () -> () = {
                    dispatch_source_cancel(timer)
                }
                closure(stop: stop)
            }
            dispatch_resume(timer)
        }
    }
    
}