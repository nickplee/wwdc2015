//
//  NSURLExtensions.swift
//  Nick Lee
//
//  Created by Nick on 4/25/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import Foundation

private let kURLPrefix = "https://itunes.apple.com/us/app/apple-store/id"

extension NSURL {
    convenience init!(appStoreID: String) {
        let urlString = kURLPrefix + appStoreID
        self.init(string: urlString)
    }
}