//
//  RootViewController.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import UIKit
import AVFoundation

private let kCellPadding = UIEdgeInsets(top: 2.5, left: 5.0, bottom: 2.5, right: 5.0)

class RootViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: Public Variables
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var videoPlayerView: VideoPlayerView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var swipeViews: [UIView]!
    
    // MARK: Private Variables
    
    private let titleAttributedString: NSAttributedString = {
        
        let attributedString = NSMutableAttributedString()
        
        attributedString.appendAttributedString(NSAttributedString(string: "Nick Lee", attributes: [
            NSFontAttributeName: UIFont(name: "Menlo", size: 60.0)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()
            ]))
        
        attributedString.mutableString.appendString("\n")
        
        attributedString.appendAttributedString(NSAttributedString(string: "Software Developer", attributes: [
            NSFontAttributeName: UIFont(name: "Menlo", size: 24.0)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()
            ]))
        
        attributedString.mutableString.appendString("\n")
        
        attributedString.appendAttributedString(NSAttributedString(string: "Jazz Pianist", attributes: [
            NSFontAttributeName: UIFont(name: "Menlo", size: 24.0)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()
            ]))
        
        return attributedString
        
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureVideoPlayer()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureCollectionViewInsets()
        videoPlayerView.play()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        type()
        animateCollectionView()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        videoPlayerView.pause()
    }
    
    // MARK: Status Bar
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK: Video Player
    
    private func configureVideoPlayer() {
        let assetOptions = [AVURLAssetPreferPreciseDurationAndTimingKey: true]
        if let url = NSBundle.mainBundle().URLForResource("spinny", withExtension: "mov"), asset = AVURLAsset(URL: url, options: assetOptions) {
            videoPlayerView.asset = asset
        }
    }
    
    // MARK: Typing label
    
    private func type() {
        titleLabel.type(titleAttributedString)
    }
    
    // MARK: UI
    
    private func configureCollectionViewInsets() {
        let interCellPadding = kCellPadding.top + kCellPadding.bottom
        let insets = UIEdgeInsets(top: view.bounds.height, left: 0.0, bottom: interCellPadding, right: 0.0)
        collectionView.contentInset = insets
        collectionViewLayout.minimumLineSpacing = interCellPadding
    }
    
    private func animateCollectionView() {
        UIView.animateWithDuration(0.3) {
            self.collectionView.alpha = 1.0
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.sharedInstance.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let content = DataManager.sharedInstance[indexPath.item]
        
        var retCell: UICollectionViewCell!
        
        // Content-specific configuration
        
        if let image = content as? Image, cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageCell", forIndexPath: indexPath) as? ImageCell {
            cell.imageView.image = image.image
            retCell = cell
        }
        else if let text = content as? Text, cell = collectionView.dequeueReusableCellWithReuseIdentifier("TextCell", forIndexPath: indexPath) as? TextCell {
            cell.textLabel.text = text.text
            retCell = cell
        }
        else if let app = content as? App, cell = collectionView.dequeueReusableCellWithReuseIdentifier("AppCell", forIndexPath: indexPath) as? AppCell {
            cell.iconImageView.image = app.icon
            cell.titleLabel.text = app.name
            cell.descriptionLabel.text = app.description
            retCell = cell
        }
        else if let contact = content as? Contact, cell = collectionView.dequeueReusableCellWithReuseIdentifier("ContactCell", forIndexPath: indexPath) as? ContactCell {
            cell.emailLabel.text = contact.email
            retCell = cell
        }
        
        // Configure the shared attributes
        
        if let baseCell = retCell as? BaseCell {
            baseCell.headerView.label = content.headerLabel
            baseCell.headerView.value = content.headerValue
        }
        
        return retCell
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = view.bounds.width - (kCellPadding.left + kCellPadding.right)
        let content = DataManager.sharedInstance[indexPath.item]
        return CGSize(width: width, height: content.height)
    }
    
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let content = DataManager.sharedInstance[indexPath.item]
        
        if let app = content as? App {
            let itsURL = NSURL(appStoreID: app.appleStoreID)
            UIApplication.sharedApplication().openURL(itsURL)
        }
        else if let contact = content as? Contact {
            sendMail(contact.email)
        }
        
    }
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let contentOffset = scrollView.contentOffset
        let offsetY = contentOffset.y + scrollView.contentInset.top
        let viewHeight = view.bounds.height
        var alpha: CGFloat = 0.0
        
        if abs(offsetY) < viewHeight {
            let ratio = ((viewHeight - offsetY) / viewHeight)
            alpha = max(0.0, min(1.0, ratio * ratio))
        }
        else {
            alpha = 0.0
        }
        
        let visible = (offsetY - 5) < 10
        for s in swipeViews {
            s.hidden = !visible
        }
        
        titleLabel.alpha = alpha
    }
    
}