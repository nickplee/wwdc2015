//
//  Text.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import Foundation
import UIKit

class Text: Content {
    // MARK: Public properties
    
    let text: String
    
    // MARK: Initialization
    
    init(text: String, headerLabel: String, headerValue: String, height: CGFloat) {
        self.text = text
        super.init(headerLabel: headerLabel, headerValue: headerValue, height: height)
    }
    
}