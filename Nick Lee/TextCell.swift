//
//  TextCell.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import UIKit

class TextCell: BaseCell {
    
    // MARK: Public properties
    
    @IBOutlet weak var textLabel: UILabel!
    
}