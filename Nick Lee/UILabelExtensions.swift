//
//  TypingLabel.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import UIKit

private let kStrokeDuration: NSTimeInterval = 0.1
private let kTypingCharacter = "_"

extension UILabel {
    
    func type(attributedString: NSAttributedString, strokeDuration: NSTimeInterval = kStrokeDuration, character: String = kTypingCharacter) {
        
        if attributedString.length == 0 {
            return
        }
        
        let update = { (pos: Int) -> Void in
            let range = NSMakeRange(0, pos)
            let attrStr = NSMutableAttributedString(attributedString: attributedString.attributedSubstringFromRange(range))
            
            if attrStr.length > 0 {
                let idx = attrStr.string.endIndex.predecessor()
                let end = attrStr.string[idx]
                if end != "\n" {
                    attrStr.mutableString.appendString(character)
                }
            }
            
            self.attributedText = attrStr
        }
        
        var pos: Int = 0

        strokeDuration.every { (stop) -> () in
            if pos <= attributedString.length {
                update(pos)
                pos++
            }
            else {
                stop()
            }
        }
        
    }
    
}