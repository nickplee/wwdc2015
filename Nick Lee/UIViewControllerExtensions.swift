//
//  UIViewControllerExtensions.swift
//  Nick Lee
//
//  Created by Nick on 4/25/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import UIKit
import MessageUI

private class ComposeDelegate: NSObject, MFMailComposeViewControllerDelegate {
    private dynamic func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}

private let staticDelegate = ComposeDelegate()

extension UIViewController {
    
    func sendMail(email: String) {
        if MFMailComposeViewController.canSendMail() {
            presentMailComposer(email)
        }
        else {
            presentEmailUnavailableAlert()
        }
    }
    
    private func presentMailComposer(email: String) {
        let composer = MFMailComposeViewController()
        composer.setToRecipients([email])
        composer.mailComposeDelegate = staticDelegate
        presentViewController(composer, animated: true, completion: nil)
    }
    
    private func presentEmailUnavailableAlert() {
        let controller = UIAlertController(title: "Error", message: "You must configure an email account before you can compose messages.", preferredStyle: .Alert)
        controller.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        presentViewController(controller, animated: true, completion: nil)
    }
    
}