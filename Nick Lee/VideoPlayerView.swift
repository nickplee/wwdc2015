//
//  VideoPlayerView.swift
//  Nick Lee
//
//  Created by Nick on 4/24/15.
//  Copyright (c) 2015 Nicholas Lee Designs, LLC. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerView: UIView {
    
    // MARK: Public Properties
    
    var asset: AVAsset? {
        didSet {
            if let theAsset = asset {
                composition = AVMutableComposition(asset: theAsset)
            }
            else {
                composition = nil
            }
        }
    }
    
    var videoGravity: String {
        get {
            return playerLayer.videoGravity
        }
        set {
            playerLayer.videoGravity = newValue
        }
    }
    
    // MARK: Private Properties
    
    private let player: AVPlayer = {
        let player = AVPlayer()
        player.actionAtItemEnd = .Pause
        return player
    }()
    
    private let playerLayer: AVPlayerLayer = {
        let layer = AVPlayerLayer()
        layer.videoGravity = AVLayerVideoGravityResizeAspectFill
        return layer
    }()
    
    private var composition: AVComposition? {
        didSet {
            if let comp = composition, item = AVPlayerItem(asset: comp) {
                player.replaceCurrentItemWithPlayerItem(item)
            }
            else {
                player.replaceCurrentItemWithPlayerItem(nil)
            }
        }
    }
    
    // MARK: Initialization

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        playerLayer.player = player
        layer.addSublayer(playerLayer)
    }
    
    // MARK: Layout
    
    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = bounds
    }
 
    // MARK: Playback
    
    func play() {
        player.play()
    }
    
    func pause() {
        player.pause()
    }
    
}